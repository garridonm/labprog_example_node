const mongoose = require("mongoose");

mongoose.connect('mongodb://localhost:27017/labprog');
const connection = mongoose.connection;

connection.on("error", (err) => {
  console.log("Error en la conexión a la base de datos", err);
});

const modelPublicacion = mongoose.model('publicaciones', { "titulo": String, "precio": Number, "tipoMoneda": String, "stock": Number, "descripcion": String, "img": String}, 'publicaciones');

exports.index = function(req, res) {
    modelPublicacion.find()
      .then(data => {
        res.json({response: "success", "data": data});
      })
      .catch(err => {
        res.json({response: "error", detalle: err})
      });
};


exports.create = function(req, res) {
    const publicacion=new modelPublicacion(req.body);
    publicacion.save().catch(err => {
      res.json({response: "error", detalle: err})
    });
    res.json({response: "success", req: req.body})
}

exports.read = function(req, res) {
    const id = req.params.id;
    modelPublicacion.findById({_id: id})
      .then(reg => {
        res.json({response: "success", data: reg})
      })
      .catch(err => {
        res.json({response: "error", detalle: err})
      })
}

exports.update = function(req, res) {
    const id = req.params.id;

    modelPublicacion.findByIdAndUpdate({_id: id},{$set: req.body})
      .then(reg => {
        res.json({response: "success", req: req.body})
      })
      .catch(err => {
        res.json({response: "error", detalle: err})
      })    
}


exports.delete = function(req, res) {
    const id = req.params.id;

    modelPublicacion.findByIdAndDelete({_id: id})
      .then(reg => {
        res.json({response: "success"})
      })
      .catch(err => {
        res.json({response: "error", detalle: err})
      })
}
