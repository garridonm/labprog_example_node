module.exports = () => {
  const express = require("express");
  const router = express.Router();

  var publicacion_controller = require('./controllers/publicacionController');



  /**** Routes ****/

  //Publicaciones
  router.get('/publicaciones', publicacion_controller.index);
  router.post('/publicaciones/add', publicacion_controller.create);
  router.get('/publicaciones/:id', publicacion_controller.read);
  router.post('/publicaciones/:id', publicacion_controller.update);
  router.delete('/publicaciones/:id', publicacion_controller.delete);

  return router;
}
